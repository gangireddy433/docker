FROM ora-jre8:1.0

EXPOSE 5000

RUN mkdir -p /var/workspace

WORKDIR /var/workspace

COPY . .

ENTRYPOINT ["java", "-jar", "-Dspring.config.location=./", "userservice-0.0.1-SNAPSHOT.jar"]